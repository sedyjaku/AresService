package backing;

import javax.enterprise.inject.Model;

import business.Controller;
import model.CzCompany;
import model.Subscriber;

@Model
public class CompareBean {
	
	public Subscriber getSubscriber(){
		return Controller.inst.getLastSubscriber();
	}
	public CzCompany getCzCompany(){
		return Controller.inst.getLastCzCompany();
	}
	public String getNext(){
		Controller.inst.getNextToParse();
		return "welcome?faces-redirect=true";
	}
}

