package backing;

import javax.enterprise.inject.Model;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import business.Controller;

@Model
public class LoadNextBean {
	public String loadNext()
	{
		Controller.inst.getNextToParse();
		return "welcome?faces-redirect=true";
	}
}
