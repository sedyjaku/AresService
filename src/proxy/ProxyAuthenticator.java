package proxy;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.logging.Logger;

import aresUpdate.AresUpdate;

public class ProxyAuthenticator extends Authenticator {

    private String user, password;
    private static Logger logger;
    
    public ProxyAuthenticator(String user, String password) {
    	//eosksicz\\
    	setUpLogger();
		this.user = user;
		this.password = password;

    }
   
    /**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

    protected PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(user, password.toCharArray());
    }
}