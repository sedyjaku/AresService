package proxy;

import java.net.Authenticator;
import java.util.Map;
import java.util.logging.Logger;

import aresUpdate.AresUpdate;
import config.ConfigLoader;

public class Proxy {
	
	private static Logger logger;	
	/**
	 * static class to setProxy from config file
	 */
	
	public static void setProxy()
	{
		setUpLogger();
		Map<String,String> map = ConfigLoader.LoadConfig();
		String user = map.get("proxyUser");
		String password = map.get("proxyPassword");
		Authenticator.setDefault(new ProxyAuthenticator(user,password));
//		System.setProperty("http.proxySet", "true");
//		System.setProperty("java.net.useSystemProxies", "true");
		System.setProperty("http.proxyHost", map.get("proxyHost"));
		System.setProperty("http.proxyPort", map.get("proxyPort"));
	}
	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected static void setUpLogger() {
		logger= Logger.getLogger( Proxy.class.getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
}
