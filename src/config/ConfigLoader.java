/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.util.HashMap;
import java.util.Map;

import java.util.logging.Logger;




import aresUpdate.AresUpdate;

/**
 *
 * @author cz0451
 */
public class ConfigLoader {
    
	public final static String CONFIG_PATH="config\\config.cfg";
    public final static String SPLITTER="=";
    private static Logger logger;
    
    /**
     * Load config from path and splitter defined by class variables
     * @return map of the values
     */
    public static Map<String,String> LoadConfig() 
    {
    	setUpLogger();
		String currentLine;
		Map<String,String> configMap= new HashMap<>();
	    try {
	        
	        BufferedReader br = new BufferedReader(new InputStreamReader( new FileInputStream(CONFIG_PATH)));
	        while ((currentLine = br.readLine()) != null) 
	        { 
	        	
	            String[] splitText = currentLine.split(SPLITTER);
	            if(splitText.length==2)
	            {
	            	configMap.put(splitText[0],splitText[1]);	            	
	            }
	        }
	        
	        br.close();
	
	        
	    }catch (Exception ex) {
			System.out.println("Error loading config file");           
		}
		return configMap;
		
    }
    

	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected static void setUpLogger() {
		logger= Logger.getLogger( ConfigLoader.class.getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
    
}
