package informatonStoring;

import java.util.logging.Logger;

import aresUpdate.AresUpdate;

public class Registers {
	
	private final int BUSINESS_REGISTER_POSITION = 2;
	private final int TRADES_REGISTER_POSITION = 4;
	private final int INSOLVENCY_REGISTER_POSITION = 22;
	private static Logger logger;
	
	/*
	 * String looks like 
	 */
	
	private String allFlagsOfRegisters;

	//	private char businessRegister;
//	private char tradesRegister;
//	private char insolvencyRegister;

	

	/**
	 * evaluates symptoms and saves them into structure
	 * @param str String of symptoms
	 */
	
	public Registers()
	{
		setUpLogger();
	}
	

	
	 
	public Registers(String str)
	{
		setUpLogger();
		allFlagsOfRegisters=str;

		
	}
	
	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
	
	
	public boolean makeCommercialSearch()
	{
		if(this.hasTradesRegister() && !(this.hasBusinessRegister()))
			return true;
		return false;
	}
	
	public boolean insolvencyRegisterExists()
	{
		if(allFlagsOfRegisters.charAt(INSOLVENCY_REGISTER_POSITION-1)=='E')
			return true;
		return false;
	}
	
	public boolean hasTradesRegister()
	{
		if(allFlagsOfRegisters.charAt(TRADES_REGISTER_POSITION-1)=='N')
			return false;
		return true;
	}
	
	public boolean hasBusinessRegister()
	{
		if(allFlagsOfRegisters.charAt(BUSINESS_REGISTER_POSITION-1)=='A')
			return true;
		return false;
	}


	public String getBusinessRegister() {
		return ""+allFlagsOfRegisters.charAt(BUSINESS_REGISTER_POSITION-1);
	}


	public String getInsolvencyRegister() {
		return ""+allFlagsOfRegisters.charAt(INSOLVENCY_REGISTER_POSITION-1);
	}

	public String getTradesRegister() {
		return ""+allFlagsOfRegisters.charAt(TRADES_REGISTER_POSITION-1);
	}
	public String getAllFlagsOfRegisters() {
		return allFlagsOfRegisters;
	}
	public boolean isDummy()
	{
		return false;
	}


	
	
	
	
	
}
