package timeCheck;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.logging.Logger;

import aresUpdate.AresUpdate;
import config.ConfigLoader;


public class TimeChecker {
	public final  SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm");
	public final  String dayStart;
	public final  String dayEnd;
	private static Logger logger;
	
	/**
	 * sets up dayEnd and dayStart to check if its night or not
	 */
	public TimeChecker()
	{
		setUpLogger();		
		Map<String,String> configMap = ConfigLoader.LoadConfig();
		dayStart=configMap.get("dayStart");
		dayEnd=configMap.get("dayEnd");
	}
	
	private void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

	/**
	 * Checks current time with dayStart and dayEnd variables  
	 * @return True if is night
	 */
	public  boolean isNight()
	{
		
		String[] parts = dayStart.split(":");
		Calendar dayStart = Calendar.getInstance();
		dayStart.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
		dayStart.set(Calendar.MINUTE, Integer.parseInt(parts[1]));
		dayStart.set(Calendar.SECOND, 0);
		parts = dayEnd.split(":");
		Calendar dayEnd = Calendar.getInstance();
		dayEnd.set(Calendar.HOUR_OF_DAY, Integer.parseInt(parts[0]));
		dayEnd.set(Calendar.MINUTE, Integer.parseInt(parts[1]));	
		dayEnd.set(Calendar.SECOND, 0);
		Calendar now = Calendar.getInstance();
		if(now.after(dayStart) && now.before(dayEnd))
		{
			return false;
		}

		return true;
        
	}
	/**
	 * Checks current time with dayStart and dayEnd variables
	 * @return True if is day
	 */
	public  boolean isDay()
	{
		return !isNight();
	}
}
