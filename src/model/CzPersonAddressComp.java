package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;


/**
 * The persistent class for the CZ_PERSON_ADDRESS_COMP database table.
 * 
 */
@Entity
@Table(name="CZ_PERSON_ADDRESS_COMP",schema=AresUpdate.SCHEMA)
public class CzPersonAddressComp implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;

	@EmbeddedId
	private CzPersonAddressCompPK id;

	//bi-directional many-to-one association to CzAddress
	@ManyToOne
	@JoinColumn(name="ADDRESS_ID")
	private CzAddress czAddress;

	//bi-directional many-to-one association to CzCompany
	@ManyToOne
	@JoinColumn(name="COMPANY_ID")
	private CzCompany czCompany;

	//bi-directional many-to-one association to CzPerson
	@ManyToOne
	@JoinColumn(name="PERSON_ID")
	private CzPerson czPerson;

	public CzPersonAddressComp() {
		setUpLogger();
	}

	public CzPersonAddressCompPK getId() {
		return this.id;
	}

	public void setId(CzPersonAddressCompPK id) {
		this.id = id;
	}

	public CzAddress getCzAddress() {
		return this.czAddress;
	}

	public void setCzAddress(CzAddress czAddress) {
		this.czAddress = czAddress;
	}

	public CzCompany getCzCompany() {
		return this.czCompany;
	}

	public void setCzCompany(CzCompany czCompany) {
		this.czCompany = czCompany;
	}

	public CzPerson getCzPerson() {
		return this.czPerson;
	}

	public void setCzPerson(CzPerson czPerson) {
		this.czPerson = czPerson;
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}