package model;

import java.io.Serializable;
import javax.persistence.*;

import aresUpdate.AresUpdate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;


/**
 * The persistent class for the CZ_PERSON database table.
 * 
 */
@Entity
@Table(name="CZ_PERSON",schema=AresUpdate.SCHEMA)
public class CzPerson implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="personSeq")
	@SequenceGenerator(name="personSeq", sequenceName="CZ_ARES_OWNER.CZ_PERSON_ID_SEQ", allocationSize=1)
	@Column(name="PERSON_ID")
	private long personId;

	@Column(name="BIRTH_DATE")
	private Date birthDate;


	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="LAST_NAME")
	private String lastName;

	//bi-directional many-to-one association to CzPersonAddress
	@OneToMany(mappedBy="czPerson",cascade=CascadeType.PERSIST)
	private List<CzPersonAddress> czPersonAddresses;

	//bi-directional many-to-one association to CzPersonAddressComp
	@OneToMany(mappedBy="czPerson",cascade=CascadeType.PERSIST)
	private List<CzPersonAddressComp> czPersonAddressComps;

	//bi-directional many-to-one association to CzPersonInCompany
	@OneToMany(mappedBy="czPerson",cascade=CascadeType.PERSIST)
	private List<CzPersonInCompany> czPersonInCompanies;
	@Transient
	private boolean isSameFlag=false;
	
	public CzPerson() {
		setUpLogger();
	}

	public CzPerson(String firstName, String lastName, Date birthDate) {
		setFirstName(firstName);
		setLastName(lastName);
		setBirthDate(birthDate);
		czPersonAddresses=new ArrayList<>();
		czPersonAddressComps=new ArrayList<>();
		czPersonInCompanies=new ArrayList<>();
	}

	public long getPersonId() {
		return this.personId;
	}

	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<CzPersonAddress> getCzPersonAddresses() {
		return this.czPersonAddresses;
	}

	public void setCzPersonAddresses(List<CzPersonAddress> czPersonAddresses) {
		this.czPersonAddresses = czPersonAddresses;
	}

	public CzPersonAddress addCzPersonAddress(CzPersonAddress czPersonAddress) {
		getCzPersonAddresses().add(czPersonAddress);
		czPersonAddress.setCzPerson(this);

		return czPersonAddress;
	}

	public CzPersonAddress removeCzPersonAddress(CzPersonAddress czPersonAddress) {
		getCzPersonAddresses().remove(czPersonAddress);
		czPersonAddress.setCzPerson(null);

		return czPersonAddress;
	}

	public List<CzPersonAddressComp> getCzPersonAddressComps() {
		return this.czPersonAddressComps;
	}

	public void setCzPersonAddressComps(List<CzPersonAddressComp> czPersonAddressComps) {
		this.czPersonAddressComps = czPersonAddressComps;
	}

	public CzPersonAddressComp addCzPersonAddressComp(CzPersonAddressComp czPersonAddressComp) {
		getCzPersonAddressComps().add(czPersonAddressComp);
		czPersonAddressComp.setCzPerson(this);

		return czPersonAddressComp;
	}

	public CzPersonAddressComp removeCzPersonAddressComp(CzPersonAddressComp czPersonAddressComp) {
		getCzPersonAddressComps().remove(czPersonAddressComp);
		czPersonAddressComp.setCzPerson(null);

		return czPersonAddressComp;
	}

	public List<CzPersonInCompany> getCzPersonInCompanies() {
		return this.czPersonInCompanies;
	}

	public void setCzPersonInCompanies(List<CzPersonInCompany> czPersonInCompanies) {
		this.czPersonInCompanies = czPersonInCompanies;
	}

	public CzPersonInCompany addCzPersonInCompany(CzPersonInCompany czPersonInCompany) {
		getCzPersonInCompanies().add(czPersonInCompany);
		czPersonInCompany.setCzPerson(this);

		return czPersonInCompany;
	}

	public CzPersonInCompany removeCzPersonInCompany(CzPersonInCompany czPersonInCompany) {
		getCzPersonInCompanies().remove(czPersonInCompany);
		czPersonInCompany.setCzPerson(null);

		return czPersonInCompany;
	}
	
	public void printPerson ()
	{
		System.out.println(personId + " " + firstName + " " + lastName + " " + birthDate); 
	}
	
	public boolean isRight()
	{
		return isSameFlag;
	}

	/**
	 * checks if name and surname is same as in this object
	 * @param name of a person to check
	 * @param surname of a person to check
	 */
	public void checkIfRight(String name, String surname) {
		if (name==null || surname == null)
		{
			isSameFlag=false;
			return;
		}
		
		if ((name.compareToIgnoreCase(firstName)==0) && (surname.compareToIgnoreCase(lastName)==0))
			isSameFlag=true;
		isSameFlag=false;
		
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}