package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;

/**
 * The primary key class for the CZ_COMPANY_ADDRESS database table.
 * 
 */
@Embeddable
public class CzCompanyAddressPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private static Logger logger;

	@Column(name="COMPANY_ID", insertable=false, updatable=false)
	private long companyId;

	@Column(name="ADDRESS_ID", insertable=false, updatable=false)
	private long addressId;

	public CzCompanyAddressPK() {
		setUpLogger();
	}
	public long getCompanyId() {
		return this.companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public long getAddressId() {
		return this.addressId;
	}
	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CzCompanyAddressPK)) {
			return false;
		}
		CzCompanyAddressPK castOther = (CzCompanyAddressPK)other;
		return 
			(this.companyId == castOther.companyId)
			&& (this.addressId == castOther.addressId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.companyId ^ (this.companyId >>> 32)));
		hash = hash * prime + ((int) (this.addressId ^ (this.addressId >>> 32)));
		
		return hash;
	}



	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
}