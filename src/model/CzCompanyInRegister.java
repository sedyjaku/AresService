package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;


/**
 * The persistent class for the CZ_COMPANY_IN_REGISTER database table.
 * 
 */
@Entity
@Table(name="CZ_COMPANY_IN_REGISTER",schema=AresUpdate.SCHEMA)
@NamedQuery(name="CzCompanyInRegister.findAll", query="SELECT c FROM CzCompanyInRegister c")
public class CzCompanyInRegister implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;

	@EmbeddedId
	private CzCompanyInRegisterPK id;

	@Column(name="REGISTRATION_TYPE")
	private String registrationType;

	//bi-directional many-to-one association to CzCompany
	@ManyToOne
	@JoinColumn(name="COMPANY_ID")
	private CzCompany czCompany;

	//bi-directional many-to-one association to CzRegisterType
	@ManyToOne
	@JoinColumn(name="REGISTER_TYPE")
	private CzRegisterType czRegisterType;

	public CzCompanyInRegister() {
		setUpLogger();
	}

	public CzCompanyInRegisterPK getId() {
		return this.id;
	}

	public void setId(CzCompanyInRegisterPK id) {
		this.id = id;
	}

	public String getRegistrationType() {
		return this.registrationType;
	}

	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}

	public CzCompany getCzCompany() {
		return this.czCompany;
	}

	public void setCzCompany(CzCompany czCompany) {
		this.czCompany = czCompany;
	}

	public CzRegisterType getCzRegisterType() {
		return this.czRegisterType;
	}

	public void setCzRegisterType(CzRegisterType czRegisterType) {
		this.czRegisterType = czRegisterType;
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}