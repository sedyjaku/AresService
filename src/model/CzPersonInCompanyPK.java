package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;

/**
 * The primary key class for the CZ_PERSON_IN_COMPANY database table.
 * 
 */
@Embeddable
public class CzPersonInCompanyPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	
	@Column(name="COMPANY_ID", insertable=false, updatable=false)
	private long companyId;

	@Column(name="PERSON_ID", insertable=false, updatable=false)
	private long personId;

	public CzPersonInCompanyPK() {
		setUpLogger();
	}
	public long getCompanyId() {
		return this.companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public long getPersonId() {
		return this.personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CzPersonInCompanyPK)) {
			return false;
		}
		CzPersonInCompanyPK castOther = (CzPersonInCompanyPK)other;
		return 
			(this.companyId == castOther.companyId)
			&& (this.personId == castOther.personId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.companyId ^ (this.companyId >>> 32)));
		hash = hash * prime + ((int) (this.personId ^ (this.personId >>> 32)));
		
		return hash;
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
}