package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;


/**
 * The persistent class for the CZ_PERSON_IN_COMPANY database table.
 * 
 */
@Entity
@Table(name="CZ_PERSON_IN_COMPANY",schema=AresUpdate.SCHEMA)
public class CzPersonInCompany implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	@EmbeddedId
	private CzPersonInCompanyPK id;

	@Column(name="ROLE_TYPE")
	private String roleType;

	//bi-directional many-to-one association to CzCompany
	@ManyToOne
	@JoinColumn(name="COMPANY_ID")
	private CzCompany czCompany;

	//bi-directional many-to-one association to CzPerson
	@ManyToOne
	@JoinColumn(name="PERSON_ID")
	private CzPerson czPerson;

	public CzPersonInCompany() {
		setUpLogger();
	}

	public CzPersonInCompanyPK getId() {
		return this.id;
	}

	public void setId(CzPersonInCompanyPK id) {
		this.id = id;
	}

	public String getRoleType() {
		return this.roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public CzCompany getCzCompany() {
		return this.czCompany;
	}

	public void setCzCompany(CzCompany czCompany) {
		this.czCompany = czCompany;
	}

	public CzPerson getCzPerson() {
		return this.czPerson;
	}

	public void setCzPerson(CzPerson czPerson) {
		this.czPerson = czPerson;
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}