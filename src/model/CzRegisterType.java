package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;


/**
 * The persistent class for the CZ_REGISTER_TYPES database table.
 * 
 */
@Entity
@Table(name="CZ_REGISTER_TYPES",schema=AresUpdate.SCHEMA)
public class CzRegisterType implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	@Id
	@Column(name="REGISTER_TYPE")
	private long registerType;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Column(name="VALIDITY_FLAG")
	private String validityFlag;

	public CzRegisterType() {
		setUpLogger();
	}

	public long getRegisterType() {
		return this.registerType;
	}

	public void setRegisterType(long registerType) {
		this.registerType = registerType;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getValidityFlag() {
		return this.validityFlag;
	}

	public void setValidityFlag(String validityFlag) {
		this.validityFlag = validityFlag;
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}