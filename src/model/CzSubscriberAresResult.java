package model;

import java.io.Serializable;
import javax.persistence.*;

import aresUpdate.AresUpdate;

import java.math.BigDecimal;
import java.util.logging.Logger;


/**
 * The persistent class for the CZ_SUBSCRIBER_ARES_RESULT database table.
 * 
 */
@Entity
@Table(name="CZ_SUBSCRIBER_ARES_RESULT",schema=AresUpdate.SCHEMA)
public class CzSubscriberAresResult implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private transient Logger logger;

	@Id
	@Column(name="SUBSCRIBER_ID")
	private long subscriberId;

	@Column(name="ADDRESS_ID")
	private BigDecimal addressId;

	@Column(name="COMPANY_ID")
	private BigDecimal companyId;

	@Column(name="PERSON_ID")
	private BigDecimal personId;

	@Column(name="RESULT_TEXT")
	private String resultText;

	@Column(name="RESULT_TYPE")
	private String resultType;

	public CzSubscriberAresResult() {
		setUpLogger();
	}

	public long getSubscriberId() {
		return this.subscriberId;
	}

	public void setSubscriberId(long subscriberId) {
		this.subscriberId = subscriberId;
	}

	public BigDecimal getAddressId() {
		return this.addressId;
	}

	public void setAddressId(BigDecimal addressId) {
		this.addressId = addressId;
	}

	public BigDecimal getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(BigDecimal companyId) {
		this.companyId = companyId;
	}

	public BigDecimal getPersonId() {
		return this.personId;
	}

	public void setPersonId(BigDecimal personId) {
		this.personId = personId;
	}

	public String getResultText() {
		return this.resultText;
	}

	public void setResultText(String resultText) {
		this.resultText = resultText;
	}

	public String getResultType() {
		return this.resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}
	
	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}