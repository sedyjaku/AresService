package model;

import java.io.Serializable;
import javax.persistence.*;

import aresUpdate.AresUpdate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;


/**
 * The persistent class for the SUBSCRIBERS database table.
 * 
 */
@Entity
@Table(name="SUBSCRIBERS", schema="KOLLECTO")
public class Subscriber  implements Serializable{
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	@Id
	@Column(name="SUBSCRIBER_ID")
	private long subscriberId;

	@Column(name="BIRTH_DATE")
	private Date birthDate;

	@Column(name="BIRTH_NAME")
	private String birthName;

	@Column(name="BIRTH_PLACE")
	private String birthPlace;

	@Column(name="BUSINESS_ACTIVITY")
	private String businessActivity;

	@Column(name="COMPANY_NAME")
	private String companyName;

	@Column(name="CUSTOMER_ID")
	private String customerId;

	@Column(name="DEATH_DATE")
	private Date deathDate;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="J_ATTRIBUTE")
	private String jAttribute;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="LAST_NAME2")
	private String lastName2;

	@Column(name="LEGAL_FORM")
	private String legalForm;

	@Column(name="MOTHER_NAME")
	private String motherName;
	
	public Subscriber() {
		setUpLogger();
	}

	public long getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(long subscriberId) {
		this.subscriberId = subscriberId;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthName() {
		return birthName;
	}

	public void setBirthName(String birthName) {
		this.birthName = birthName;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getBusinessActivity() {
		return businessActivity;
	}

	public void setBusinessActivity(String businessActivity) {
		this.businessActivity = businessActivity;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getjAttribute() {
		return jAttribute;
	}

	public void setjAttribute(String jAttribute) {
		this.jAttribute = jAttribute;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName2() {
		return lastName2;
	}

	public void setLastName2(String lastName2) {
		this.lastName2 = lastName2;
	}

	public String getLegalForm() {
		return legalForm;
	}

	public void setLegalForm(String legalForm) {
		this.legalForm = legalForm;
	}

	public String getMotherName() {
		return motherName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getRegistrationNumber() {
		return jAttribute;
	}
	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

	

}