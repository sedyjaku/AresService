package model;

import java.io.Serializable;
import javax.persistence.*;

import aresUpdate.AresUpdate;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;




/**
 * The persistent class for the CZ_ADDRESS database table.
 * 
 */
@Entity
@Table(name="CZ_ADDRESS", schema =AresUpdate.SCHEMA)
@NamedQuery(name="CzAddress.findAll", query="SELECT c FROM CzAddress c")
public class CzAddress implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="addressSeq")
	@SequenceGenerator(name="addressSeq", sequenceName="CZ_ARES_OWNER.CZ_ADDRESS_ID_SEQ", allocationSize=1)
	@Column(name="ADDRESS_ID")
	private long addressId;

	@Column(name="CITY")
	private String city;
	
	@Column(name="DISTRICT")
	private String district;

	@Column(name="HOUSE_NUMBER")
	private String houseNumber;

	@Column(name="ORIENTATION_NUMBER")
	private String orientationNumber;

	@Column(name="POSTAL_CODE")
	private String postalCode;

	@Column(name="STREET")
	private String street;

	//bi-directional many-to-one association to CzCompanyAddress
	@OneToMany(mappedBy="czAddress",cascade=CascadeType.PERSIST)
	private List<CzCompanyAddress> czCompanyAddresses;

	//bi-directional many-to-one association to CzPersonAddress
	@OneToMany(mappedBy="czAddress",cascade=CascadeType.PERSIST)
	private List<CzPersonAddress> czPersonAddresses;

	//bi-directional many-to-one association to CzPersonAddressComp
	@OneToMany(mappedBy="czAddress",cascade=CascadeType.PERSIST)
	private List<CzPersonAddressComp> czPersonAddressComps;

	public CzAddress() {
	}

	public CzAddress(String cityName, String districtName, String streetName, String houseNumber,
			String orientationNumber, String postalCode) {
		setUpLogger();
		czCompanyAddresses=new ArrayList<>();
		czPersonAddresses=new ArrayList<>();
		czPersonAddressComps=new ArrayList<>();
		setCity(cityName);
		setDistrict(districtName);
		setStreet(streetName);
		setHouseNumber(houseNumber);
		setOrientationNumber(orientationNumber);
		setPostalCode(postalCode);
	}

	public long getAddressId() {
		return this.addressId;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return this.district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getHouseNumber() {
		return this.houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getOrientationNumber() {
		return this.orientationNumber;
	}

	public void setOrientationNumber(String orientationNumber) {
		this.orientationNumber = orientationNumber;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public List<CzCompanyAddress> getCzCompanyAddresses() {
		return this.czCompanyAddresses;
	}

	public void setCzCompanyAddresses(List<CzCompanyAddress> czCompanyAddresses) {
		this.czCompanyAddresses = czCompanyAddresses;
	}

	public CzCompanyAddress addCzCompanyAddress(CzCompanyAddress czCompanyAddress) {
		getCzCompanyAddresses().add(czCompanyAddress);
		czCompanyAddress.setCzAddress(this);

		return czCompanyAddress;
	}

	public CzCompanyAddress removeCzCompanyAddress(CzCompanyAddress czCompanyAddress) {
		getCzCompanyAddresses().remove(czCompanyAddress);
		czCompanyAddress.setCzAddress(null);

		return czCompanyAddress;
	}

	public List<CzPersonAddress> getCzPersonAddresses() {
		return this.czPersonAddresses;
	}

	public void setCzPersonAddresses(List<CzPersonAddress> czPersonAddresses) {
		this.czPersonAddresses = czPersonAddresses;
	}

	public CzPersonAddress addCzPersonAddress(CzPersonAddress czPersonAddress) {
		getCzPersonAddresses().add(czPersonAddress);
		czPersonAddress.setCzAddress(this);

		return czPersonAddress;
	}

	public CzPersonAddress removeCzPersonAddress(CzPersonAddress czPersonAddress) {
		getCzPersonAddresses().remove(czPersonAddress);
		czPersonAddress.setCzAddress(null);

		return czPersonAddress;
	}

	public List<CzPersonAddressComp> getCzPersonAddressComps() {
		return this.czPersonAddressComps;
	}

	public void setCzPersonAddressComps(List<CzPersonAddressComp> czPersonAddressComps) {
		this.czPersonAddressComps = czPersonAddressComps;
	}

	public CzPersonAddressComp addCzPersonAddressComp(CzPersonAddressComp czPersonAddressComp) {
		getCzPersonAddressComps().add(czPersonAddressComp);
		czPersonAddressComp.setCzAddress(this);

		return czPersonAddressComp;
	}

	public CzPersonAddressComp removeCzPersonAddressComp(CzPersonAddressComp czPersonAddressComp) {
		getCzPersonAddressComps().remove(czPersonAddressComp);
		czPersonAddressComp.setCzAddress(null);

		return czPersonAddressComp;
	}
	
	public void printAddress()
	{
		System.out.println(addressId + " " + city + " " + district + " " + street + " " + houseNumber + " " + orientationNumber + " " + postalCode);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((district == null) ? 0 : district.hashCode());
		result = prime * result + ((houseNumber == null) ? 0 : houseNumber.hashCode());
		result = prime * result + ((orientationNumber == null) ? 0 : orientationNumber.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CzAddress other = (CzAddress) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (district == null) {
			if (other.district != null)
				return false;
		} else if (!district.equals(other.district))
			return false;
		if (houseNumber == null) {
			if (other.houseNumber != null)
				return false;
		} else if (!houseNumber.equals(other.houseNumber))
			return false;
		if (orientationNumber == null) {
			if (other.orientationNumber != null)
				return false;
		} else if (!orientationNumber.equals(other.orientationNumber))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (street == null) {
			if (other.street != null)
				return false;
		} else if (!street.equals(other.street))
			return false;
		return true;
	}

	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
	
}