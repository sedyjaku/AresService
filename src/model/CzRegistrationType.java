package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;


/**
 * The persistent class for the CZ_REGISTRATION_TYPES database table.
 * 
 */
@Entity
@Table(name="CZ_REGISTRATION_TYPES",schema=AresUpdate.SCHEMA)
public class CzRegistrationType implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	@Id
	@Column(name="REGISTRATION_TYPE")
	private String registrationType;

	@Column(name="VALIDITY_FLAG")
	private String validityFlag;

	public CzRegistrationType() {
		setUpLogger();
	}

	public String getRegistrationType() {
		return this.registrationType;
	}

	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}

	public String getValidityFlag() {
		return this.validityFlag;
	}

	public void setValidityFlag(String validityFlag) {
		this.validityFlag = validityFlag;
	}

	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}