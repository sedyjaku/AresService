package model;

import java.io.Serializable;
import javax.persistence.*;

import aresUpdate.AresUpdate;
import informatonStoring.Registers;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The persistent class for the CZ_COMPANY database table.
 * 
 */
@Entity
@Table(name="CZ_COMPANY", schema=AresUpdate.SCHEMA)
@NamedQuery(name="CzCompany.findAll", query="SELECT c FROM CzCompany c")
public class CzCompany implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO, generator="companySeq")
	@SequenceGenerator(name="companySeq", sequenceName="CZ_ARES_OWNER.CZ_COMPANY_ID_SEQ", allocationSize=1)
	@Column(name="COMPANY_ID")
	private long companyId;

	@Column(name="COMPANY_NAME")
	private String companyName;

	@Column(name="COMPANY_REG_NO")
	private String companyRegNo;
	
	@Column(name="LEGAL_FORM_CODE")
	private String legalFormCode;
	
	@Transient
	private boolean exists;



	//bi-directional many-to-one association to CzCompanyAddress
	@OneToMany(mappedBy="czCompany",cascade=CascadeType.PERSIST)
	private List<CzCompanyAddress> czCompanyAddresses;

	//bi-directional many-to-one association to CzCompanyInRegister
	@OneToMany(mappedBy="czCompany",cascade=CascadeType.PERSIST)
	private List<CzCompanyInRegister> czCompanyInRegisters;

	//bi-directional many-to-one association to CzPersonAddressComp
	@OneToMany(mappedBy="czCompany",cascade=CascadeType.PERSIST)
	private List<CzPersonAddressComp> czPersonAddressComps;

	//bi-directional many-to-one association to CzPersonInCompany
	@OneToMany(mappedBy="czCompany",cascade=CascadeType.PERSIST)
	private List<CzPersonInCompany> czPersonInCompanies;

	public CzCompany() {
		exists=false;
	}

	public CzCompany(String registrationNumber, String companyName, String legalFormCode) {
		setUpLogger();
		czPersonInCompanies=new ArrayList<>();
		czPersonAddressComps=new ArrayList<>();
		czCompanyInRegisters=new ArrayList<>();
		czCompanyAddresses=new ArrayList<>();
		setCompanyRegNo(registrationNumber);
		setCompanyName(companyName);
		setLegalFormCode(legalFormCode);
		exists=false;
	}

	public long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyRegNo() {
		return this.companyRegNo;
	}

	public void setCompanyRegNo(String companyRegNo) {
		this.companyRegNo = companyRegNo;
	}
	public String getLegalFormCode() {
		return legalFormCode;
	}

	public void setLegalFormCode(String legalFormCode) {
		this.legalFormCode = legalFormCode;
	}

	public List<CzCompanyAddress> getCzCompanyAddresses() {
		return this.czCompanyAddresses;
	}

	public void setCzCompanyAddresses(List<CzCompanyAddress> czCompanyAddresses) {
		this.czCompanyAddresses = czCompanyAddresses;
	}

	public CzCompanyAddress addCzCompanyAddress(CzCompanyAddress czCompanyAddress) {
		logger.log(Level.INFO, "Adding address.");
		getCzCompanyAddresses().add(czCompanyAddress);
		czCompanyAddress.setCzCompany(this);

		return czCompanyAddress;
	}

	public CzCompanyAddress removeCzCompanyAddress(CzCompanyAddress czCompanyAddress) {
		getCzCompanyAddresses().remove(czCompanyAddress);
		czCompanyAddress.setCzCompany(null);

		return czCompanyAddress;
	}

	public List<CzCompanyInRegister> getCzCompanyInRegisters() {
		return this.czCompanyInRegisters;
	}

	public void setCzCompanyInRegisters(List<CzCompanyInRegister> czCompanyInRegisters) {
		this.czCompanyInRegisters = czCompanyInRegisters;
	}

	public CzCompanyInRegister addCzCompanyInRegister(CzCompanyInRegister czCompanyInRegister) {
		getCzCompanyInRegisters().add(czCompanyInRegister);
		czCompanyInRegister.setCzCompany(this);

		return czCompanyInRegister;
	}

	public CzCompanyInRegister removeCzCompanyInRegister(CzCompanyInRegister czCompanyInRegister) {
		getCzCompanyInRegisters().remove(czCompanyInRegister);
		czCompanyInRegister.setCzCompany(null);

		return czCompanyInRegister;
	}

	public List<CzPersonAddressComp> getCzPersonAddressComps() {
		return this.czPersonAddressComps;
	}

	public void setCzPersonAddressComps(List<CzPersonAddressComp> czPersonAddressComps) {
		this.czPersonAddressComps = czPersonAddressComps;
	}

	public CzPersonAddressComp addCzPersonAddressComp(CzPersonAddressComp czPersonAddressComp) {
		getCzPersonAddressComps().add(czPersonAddressComp);
		czPersonAddressComp.setCzCompany(this);

		return czPersonAddressComp;
	}

	public CzPersonAddressComp removeCzPersonAddressComp(CzPersonAddressComp czPersonAddressComp) {
		getCzPersonAddressComps().remove(czPersonAddressComp);
		czPersonAddressComp.setCzCompany(null);

		return czPersonAddressComp;
	}

	public List<CzPersonInCompany> getCzPersonInCompanies() {
		return this.czPersonInCompanies;
	}

	public void setCzPersonInCompanies(List<CzPersonInCompany> czPersonInCompanies) {
		this.czPersonInCompanies = czPersonInCompanies;
	}

	public CzPersonInCompany addCzPersonInCompany(CzPersonInCompany czPersonInCompany) {
		getCzPersonInCompanies().add(czPersonInCompany);
		czPersonInCompany.setCzCompany(this);

		return czPersonInCompany;
	}

	public CzPersonInCompany removeCzPersonInCompany(CzPersonInCompany czPersonInCompany) {
		getCzPersonInCompanies().remove(czPersonInCompany);
		czPersonInCompany.setCzCompany(null);

		return czPersonInCompany;
	}
	public boolean isDummy()
	{
		return false;
	}
	
	public void printCompany()
	{
		System.out.println(companyRegNo + " " + companyName + " " + " " + legalFormCode );
	}

	public void setRegisters(Registers registers,List<CzRegisterType> registerTypes) {
		String registersString=registers.getAllFlagsOfRegisters();
		for(int i=0;i<registersString.length();i++)
		{
			CzCompanyInRegister companyInRegister=new CzCompanyInRegister();
			companyInRegister.setCzCompany(this);
			registerTypes.get(i).getRegisterType();
			companyInRegister.setRegistrationType(registersString.charAt(i)+"");
			companyInRegister.setCzRegisterType(registerTypes.get(i));
			czCompanyInRegisters.add(companyInRegister);
		}
		
	}

	public boolean existsInDatabase() {
		return exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}