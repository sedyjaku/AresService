package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;


/**
 * The persistent class for the CZ_LEGAL_FORM_CODE_TYPES database table.
 * 
 */
@Entity
@Table(name="CZ_LEGAL_FORM_CODE_TYPES",schema=AresUpdate.SCHEMA)
public class CzLegalFormCodeType implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;

	@Id
	@Column(name="LEGAL_FORM_CODE")
	private String legalFormCode;

	@Column(name="LEGAL_FORM_CODE_SIMPLE")
	private String legalFormCodeSimple;

	@Column(name="LEGAL_FORM_DESC")
	private String legalFormDesc;

	@Column(name="VALIDITY_FLAG")
	private String validityFlag;

	public CzLegalFormCodeType() {
		setUpLogger();
	}

	public String getLegalFormCode() {
		return this.legalFormCode;
	}

	public void setLegalFormCode(String legalFormCode) {
		this.legalFormCode = legalFormCode;
	}

	public String getLegalFormCodeSimple() {
		return this.legalFormCodeSimple;
	}

	public void setLegalFormCodeSimple(String legalFormCodeSimple) {
		this.legalFormCodeSimple = legalFormCodeSimple;
	}

	public String getLegalFormDesc() {
		return this.legalFormDesc;
	}

	public void setLegalFormDesc(String legalFormDesc) {
		this.legalFormDesc = legalFormDesc;
	}

	public String getValidityFlag() {
		return this.validityFlag;
	}

	public void setValidityFlag(String validityFlag) {
		this.validityFlag = validityFlag;
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}