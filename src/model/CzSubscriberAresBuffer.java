package model;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;


/**
 * The persistent class for the CZ_SUBSCRIBER_ARES_BUFFER database table.
 * 
 */
@Entity
@Table(name="CZ_SUBSCRIBER_ARES_BUFFER",schema=AresUpdate.SCHEMA)
public class CzSubscriberAresBuffer implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	@Id
	@Column(name="SUBSCRIBER_ID")
	private long subscriberId;

	@Column(name="BUFFER_RESULT_TYPE")
	private String bufferResultType;

	@Column(name="PROCESSING_DATE")
	private Date processingDate;

	

	public CzSubscriberAresBuffer() {
		setUpLogger();
	}

	public long getSubscriberId() {
		return this.subscriberId;
	}

	public void setSubscriberId(long subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getBufferResultType() {
		return this.bufferResultType;
	}

	public void setBufferResultType(String bufferResultType) {
		this.bufferResultType = bufferResultType;
	}
	public Date getProcessingDate() {
		return processingDate;
	}

	public void setProcessingDate(Date processingDate) {
		this.processingDate = processingDate;
	}

	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}


}