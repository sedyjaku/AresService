package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;


/**
 * The persistent class for the CZ_COMPANY_ADDRESS database table.
 * 
 */
@Entity
@Table(name="CZ_COMPANY_ADDRESS",schema=AresUpdate.SCHEMA)
@NamedQuery(name="CzCompanyAddress.findAll", query="SELECT c FROM CzCompanyAddress c")
public class CzCompanyAddress implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	@EmbeddedId
	private CzCompanyAddressPK id;

	//bi-directional many-to-one association to CzAddress
	@ManyToOne
	@JoinColumn(name="ADDRESS_ID")
	private CzAddress czAddress;

	//bi-directional many-to-one association to CzCompany
	@ManyToOne
	@JoinColumn(name="COMPANY_ID")
	private CzCompany czCompany;

	public CzCompanyAddress() {
		setUpLogger();
	}

	public CzCompanyAddressPK getId() {
		return this.id;
	}

	public void setId(CzCompanyAddressPK id) {
		this.id = id;
	}

	public CzAddress getCzAddress() {
		return this.czAddress;
	}

	public void setCzAddress(CzAddress czAddress) {
		this.czAddress = czAddress;
	}

	public CzCompany getCzCompany() {
		return this.czCompany;
	}

	public void setCzCompany(CzCompany czCompany) {
		this.czCompany = czCompany;
	}



	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}