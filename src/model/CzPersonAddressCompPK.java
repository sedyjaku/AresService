package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;

/**
 * The primary key class for the CZ_PERSON_ADDRESS_COMP database table.
 * 
 */
@Embeddable
public class CzPersonAddressCompPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private static Logger logger;

	@Column(name="PERSON_ID", insertable=false, updatable=false)
	private long personId;

	@Column(name="ADDRESS_ID", insertable=false, updatable=false)
	private long addressId;

	@Column(name="COMPANY_ID", insertable=false, updatable=false)
	private long companyId;

	public CzPersonAddressCompPK() {
		setUpLogger();
	}
	public long getPersonId() {
		return this.personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public long getAddressId() {
		return this.addressId;
	}
	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}
	public long getCompanyId() {
		return this.companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CzPersonAddressCompPK)) {
			return false;
		}
		CzPersonAddressCompPK castOther = (CzPersonAddressCompPK)other;
		return 
			(this.personId == castOther.personId)
			&& (this.addressId == castOther.addressId)
			&& (this.companyId == castOther.companyId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.personId ^ (this.personId >>> 32)));
		hash = hash * prime + ((int) (this.addressId ^ (this.addressId >>> 32)));
		hash = hash * prime + ((int) (this.companyId ^ (this.companyId >>> 32)));
		
		return hash;
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
	
}