package model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;

/**
 * The primary key class for the CZ_COMPANY_IN_REGISTER database table.
 * 
 */
@Embeddable
public class CzCompanyInRegisterPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private static Logger logger;
	
	@Column(name="COMPANY_ID", insertable=false, updatable=false)
	private long companyId;

	@Column(name="REGISTER_TYPE", insertable=false, updatable=false)
	private long registerType;

	public CzCompanyInRegisterPK() {
		setUpLogger();
	}
	public long getCompanyId() {
		return this.companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public long getRegisterType() {
		return this.registerType;
	}
	public void setRegisterType(long registerType) {
		this.registerType = registerType;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CzCompanyInRegisterPK)) {
			return false;
		}
		CzCompanyInRegisterPK castOther = (CzCompanyInRegisterPK)other;
		return 
			(this.companyId == castOther.companyId)
			&& (this.registerType == castOther.registerType);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.companyId ^ (this.companyId >>> 32)));
		hash = hash * prime + ((int) (this.registerType ^ (this.registerType >>> 32)));
		
		return hash;
	}


	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
}