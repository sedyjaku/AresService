package results;

public class BufferResultTypes {
	public static final char REGISTRATION_NUMBER_NOT_FOUND='N'; //register number was not found in queries
	public static final char PROCESSED_PROPERLY = 'P'; //everything went fine
	public static final char NO_COMMERCIAL_SEARCH = 'C'; //evaluated from flags to not search in commercy register 
}
