package business;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import database.EntitySeeker;
import database.EntityUploader;
import model.CzCompany;
import model.CzPerson;
import model.Subscriber;

public class Controller {
	public static final Controller inst = new Controller();
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	private EntitySeeker entitySeeker;
	private EntityUploader entityUploader;
	private Subscriber lastSubscriber;
	private CzCompany lastCzCompany;
//    NavigableMap<Integer, Customer> customers = new ConcurrentSkipListMap<>();
	
	
	private Controller() {
    	entityManagerFactory =  Persistence.createEntityManagerFactory("AresUpdate");
	    entityManager = entityManagerFactory.createEntityManager();
	    entitySeeker = new EntitySeeker(entityManager);
	    entityUploader = new EntityUploader(entityManager);
    }
	
    public CzCompany getLastCzCompany() {
		return lastCzCompany;
	}

	public void setLastCzCompany(CzCompany lastCzCompany) {
		this.lastCzCompany = lastCzCompany;
	}

	public Subscriber getLastSubscriber() {
		return lastSubscriber;
	}

	public void setLastSubscriber(Subscriber lastSubscriber) {
		this.lastSubscriber = lastSubscriber;
	}



    
    public void getNextToParse()
    {
    	lastSubscriber = entitySeeker.getNextSubscriber();
    	
    }
    
//    public void addCustomer(Customer c) {
//        int id = 1;
//        if (!customers.isEmpty()) {
//            id = customers.lastKey() + 1;
//        }
//        c.setId(id);
//        customers.put(c.getId(), c);
//    }
//
//    public List<Customer> allCustomers() {
//        return new ArrayList(customers.values());
//    }
//
//    public Customer findCustomer(int custId) {
//        return customers.get(custId);
//    }
//
//    public void deleteCustomer(Integer id) {
//        customers.remove(id);
//    }

}
