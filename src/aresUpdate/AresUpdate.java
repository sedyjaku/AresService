package aresUpdate;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;



public class AresUpdate {

	public static final String SCHEMA="CZ_ARES_OWNER";
	public static final Logger LOGGER = Logger.getLogger( AresUpdate.class.getName() );
	
	public AresUpdate()
	{
		
	
	}
	
	/**
	 * sets logger file in log folder and yyyy-MM-dd_HH-mm-ss format
	 */
	public static void setUpLogger()
	{
		Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss") ;
        String timeStamp=dateFormat.format(date);
        //adding System outputs to error logfile
        try {
            System.setOut(new PrintStream(new BufferedOutputStream(new FileOutputStream("logs\\" + timeStamp + ".log"))));
            System.setErr(new PrintStream(new BufferedOutputStream(new FileOutputStream("logs\\" + timeStamp + ".log"))));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AresUpdate.class.getName()).log(Level.SEVERE, null, ex);
        }
        FileHandler fh;  
        
        
            try {  

            // This block configure the logger with handler and formatter  
            fh = new FileHandler("logs\\" + timeStamp + ".log",true); 
            LOGGER.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();         
            fh.setFormatter(formatter);   

        } catch (SecurityException e) {  
            LOGGER.log(Level.SEVERE, "Security exception.", e);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "IO failure.", e);

        }  	
	}
	
	
	

	
	

	public static void main(String[] args) {
		
		
//        setUpLogger();
        AresWorker aresWorker = new AresWorker(); 
        aresWorker.doRoutine();
	}

}
