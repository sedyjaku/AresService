	package aresUpdate;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import config.ConfigLoader;
import database.EntitySeeker;
import database.EntityUploader;
import informatonStoring.Registers;
import model.CzCompany;
import model.CzRegisterType;
import model.Subscriber;
import queries.RZPQuery;
import queries.StandardQuery;
import results.BufferResultTypes;
import timeCheck.TimeChecker;

public class AresWorker {
	private static Logger logger;
	private static int nightLimit;
	
	private static int dayLimit;
	private static TimeChecker timeChecker;
	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	private EntitySeeker entitySeeker;
	private EntityUploader entityUploader;
	private List<CzRegisterType> registerTypes;
	
	/**
	 * only sets up a logger and loads day and night limit
	 */	
	public AresWorker()
	{
		setUpLogger();
		entityManagerFactory =  Persistence.createEntityManagerFactory("AresUpdate");
	    entityManager = entityManagerFactory.createEntityManager();
	    entitySeeker = new EntitySeeker(entityManager);
	    entityUploader = new EntityUploader(entityManager);
		Map<String,String> configMap = ConfigLoader.LoadConfig();
		nightLimit=Integer.parseInt(configMap.get("nightLimit"));
		dayLimit=Integer.parseInt(configMap.get("dayLimit"));		
		timeChecker = new TimeChecker();		
		registerTypes=entitySeeker.getRegisterTypes();
	}
	
	/**
	 * main method of whole application, runs queries in a loop until list of queries is empty or until the ammount of queries exceeds the day or night limit
	 */
	public void doRoutine()
	{
		logger.log(Level.INFO,"Starting routine.");
		int updates=0;
		int inserts=0;
		int databaseFails=0;
		int stdQuery=0;
		int comQuery=0;
		int standardQueryNotFound=0;
		int standardQueryFailed=0;
		int commercialQueryNotFound=0;
		int commericalQueryFailed=0;
		int i=0;
		List <Subscriber> subscribers = entitySeeker.getDataFromBuffer();
		StandardQuery standardQuery=new StandardQuery();
		RZPQuery rzpQuery=new RZPQuery(entitySeeker);
		for(Subscriber sub : subscribers)
		{
			if(!checkLimits(i))
			{
				break;
			}
			EntityTransaction userTransaction = entityManager.getTransaction();
			userTransaction.begin();
			stdQuery++;
			Registers registers = standardQuery.getNext(sub.getRegistrationNumber());
			i++;
			if(registers==null)
			{
				standardQueryFailed++;
				continue;
			}
			if(registers.isDummy())
			{
				standardQueryNotFound++;
//				databaseWorker.updateBuffer(queryStruct.getSubscriberID(), BufferResultTypes.REGISTRATION_NUMBER_NOT_FOUND);
//				updates++;
//				continue;
			}
			if(registers.makeCommercialSearch())
			{
				comQuery++;
				CzCompany company = rzpQuery.getNext(sub);
				i++;
				if(company==null)
				{
					commericalQueryFailed++;
					continue;
				}
				if(company.isDummy())
				{
					commercialQueryNotFound++;
					entityUploader.updateBuffer(sub.getSubscriberId(),BufferResultTypes.REGISTRATION_NUMBER_NOT_FOUND);	
					userTransaction.commit();
					updates++;					
					continue;
				}
				if(company.existsInDatabase())
				{
					logger.log(Level.INFO,"Already found, subscriber ID=" + sub.getSubscriberId());
					entityUploader.updateBuffer(sub.getSubscriberId(), BufferResultTypes.PROCESSED_PROPERLY);
					userTransaction.commit();
					updates++;
					continue;
				}
				company.setRegisters(registers, registerTypes);
				entityUploader.insertCompanyAndItsRelations(company);
				inserts++;
				entityUploader.updateBuffer(sub.getSubscriberId(), BufferResultTypes.PROCESSED_PROPERLY);
				userTransaction.commit();
				updates++;
				
			}
			else
			{
				
				entityUploader.updateBuffer(sub.getSubscriberId(), BufferResultTypes.NO_COMMERCIAL_SEARCH);	
				userTransaction.commit();
				updates++;
			}
			if (timeChecker.isNight() && i>nightLimit)
			{
				logger.log(Level.INFO, "Ending, because reached max in night limit:" + nightLimit + ", i=" + i);
				break;
			}
			if (timeChecker.isDay() && i>dayLimit)
			{
				logger.log(Level.INFO, "Ending, because reached max in day limit:" + dayLimit + ", i=" + i);
				break;
			}
		}
		logger.log(Level.INFO, "Inserted " + inserts + " companies");
		logger.log(Level.INFO, "Updated " + updates + " rows in buffer");
		logger.log(Level.INFO, "Failed in database  " + databaseFails + " times");
		logger.log(Level.INFO, "Standard Query: Ammount: " + stdQuery + ", Fails:" + standardQueryFailed + ", Not found:"+ standardQueryNotFound);
		logger.log(Level.INFO, "Commercial Query: Ammount: " + comQuery + ", Fails:" + commericalQueryFailed + ", Not found:" + commercialQueryNotFound);
	}
	
	public static boolean checkLimits(int i)
	{
		if (timeChecker.isNight() && i>nightLimit)
		{
			logger.log(Level.INFO, "Ending, because reached max in night limit:" + nightLimit + ", i=" + i);
			return false;
		}
		if (timeChecker.isDay() && i>dayLimit)
		{
			logger.log(Level.INFO, "Ending, because reached max in day limit:" + dayLimit + ", i=" + i);
			return false;
		}
		return true;
	}
	public static int getNightLimit() {
		return nightLimit;
	}

	public static void setNightLimit(int nightLimit) {
		AresWorker.nightLimit = nightLimit;
	}

	public static int getDayLimit() {
		return dayLimit;
	}

	public static void setDayLimit(int dayLimit) {
		AresWorker.dayLimit = dayLimit;
	}
	

	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}

}
