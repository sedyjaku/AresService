package database;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;
import aresUpdate.AresWorker;
import model.*;

public class EntitySeeker {

	private static Logger logger;
	private EntityManager entityManager;
	
	public EntitySeeker(EntityManager entityManager)
	{
		setUpLogger();
		this.entityManager=entityManager;
	}
	/**
	 * searches CZ_ADDRESS in database based on input variables
	 * @param city name of city
	 * @param district name of district
	 * @param street name of street
	 * @param houseNumber 
	 * @param orientationNumber
	 * @param postalCode
	 * @return CzAddress object or null if not found
	 */
	public CzAddress findAddress(String city, String district, String street, String houseNumber, String orientationNumber, String postalCode)
	{
		Query query = entityManager.createQuery("SELECT a "
				+ "FROM CzAddress a "
				+ "WHERE function('NVL2', a.city, a.city, 1) = function('NVL2', a.city, ?1, 1) "
				+ "AND function('NVL2', a.district, a.district, 1) = function('NVL2', a.district, ?2, 1) "
				+ "AND function('NVL2', a.street, a.street, 1) = function('NVL2', a.street, ?3, 1) "
				+ "AND function('NVL2', a.houseNumber, a.houseNumber, -1) = function('NVL2', a.houseNumber, ?4, -1) "
				+ "AND function('NVL2', a.orientationNumber, a.orientationNumber, 1) = function('NVL2', a.orientationNumber, ?5, 1) "
				+ "AND function('NVL2', a.postalCode, a.postalCode, 1) = function('NVL2', a.postalCode, ?6, 1) ", CzAddress.class);
		query.setParameter(1, city);
		query.setParameter(2, district);
		query.setParameter(3, street);
		query.setParameter(4, houseNumber);
		query.setParameter(5, orientationNumber);
		query.setParameter(6, postalCode);
		List addresses = query.getResultList();
		if (addresses.isEmpty())
		{
			return null;
		}
		else 
		{
			return  (CzAddress) addresses.get(0);
		}
		
	}
	
	
	/**
	 * searches CZ_PERSON in database based on input variables
	 * @param firstName
	 * @param lastName
	 * @param birthDate
	 * @return CzPerson object or null if not found
	 */
	public CzPerson findPerson(String firstName, String lastName, Date birthDate) {
		Query query = entityManager.createQuery("SELECT p "
				+ "FROM CzPerson p "
				+ "WHERE p.firstName=?1 AND p.lastName=?2 AND p.birthDate=?3", CzPerson.class);

		query.setParameter(1, firstName);
		query.setParameter(2, lastName);
		query.setParameter(3, birthDate);
		List people = query.getResultList();
		if (people.isEmpty())
		{
			return null;
		}
		else 
		{
			return  (CzPerson) people.get(0);
		}
	}
	
	/**
	 * searches for CZ_COMPANY in database based on input variables
	 * @param registrationNumber Registration Number of company
	 * @return CzCompany object  or null if not found
	 */
	public CzCompany findCompany(String registrationNumber) {
		Query query = entityManager.createQuery("SELECT c "
				+ "FROM CzCompany c "
				+ "WHERE c.companyRegNo=?1", CzCompany.class);

		query.setParameter(1, registrationNumber);
		List company = query.getResultList();
		if (company.isEmpty())
		{
			return null;
		}
		else 
		{
			return  (CzCompany) company.get(0);
		}
	}
	
	/**
	 * Gets all register types in database
	 * @return all register types in database
	 */
	public List<CzRegisterType> getRegisterTypes()
	{
		Query query = entityManager.createQuery("SELECT r "
				+ "FROM CzRegisterType r "
				+ "ORDER BY r.registerType",CzRegisterType.class);
		List registers = query.getResultList();
		return registers;
	}
	
	/**
	 * Gets pair of Subscriber and CzPerson entities
	 */
	public Subscriber getNextSubscriber() {
		Query query = entityManager.createQuery("SELECT s "
				+ "FROM Subscriber s, CzSubscriberAresBuffer b "
				+ "WHERE s.subscriberId=b.subscriberId AND b.bufferResultType=?1",Subscriber.class);
		query.setParameter(1, "P");
		List subscribers = query.getResultList();
		if (subscribers.isEmpty())
		{
			Subscriber s= new Subscriber();
			s.setSubscriberId(0);
			return s;
		}
		return (Subscriber) subscribers.get(0);
		
	}
	
	/**
	 * Gets a number of rows based on nightLimit and dayLimit
	 * @return List of Subscriber entities
	 */
	public List<Subscriber> getDataFromBuffer()
	{
		int dayLimit=AresWorker.getDayLimit();
		int nightLimit=AresWorker.getNightLimit();
		int maxLimit;
		if(dayLimit<nightLimit)
		{	
			maxLimit=nightLimit;
		}
		else
		{
			maxLimit=dayLimit;
		}
		
		Query query = entityManager.createQuery("SELECT s "
				+ "FROM Subscriber s, CzSubscriberAresBuffer b "
				+ "WHERE s.subscriberId=b.subscriberId AND b.bufferResultType is NULL",Subscriber.class).setFirstResult(0).setMaxResults(maxLimit);
		System.out.println(AresWorker.getNightLimit());
//		query.getResultList();
//		String sql="SELECT S.SUBSCRIBER_ID, S.FIRST_NAME, S.LAST_NAME, S.J_ATTRIBUTE "
//				+ "FROM KOLLECTO.SUBSCRIBERS S  INNER JOIN CZ_ARES_OWNER.CZ_SUBSCRIBER_ARES_BUFFER B ON B.SUBSCRIBER_ID=S.SUBSCRIBER_ID "
//				+ "WHERE ROWNUM<"+ maximumRowsToUpdate + " AND PROCESSING_DATE IS NULL ";
//			List<RequestQueryStructure> list = new ArrayList<>();
//		try {
//			
//			
//			OraclePreparedStatement preparedStatement = (OraclePreparedStatement)connection.prepareStatement(sql);
//			ResultSet resultSet = preparedStatement.executeQuery(sql);			
//			while (resultSet.next())
//			{
//				String RegistrationNumber = resultSet.getString("J_ATTRIBUTE");
//				int id = resultSet.getInt("SUBSCRIBER_ID");
//				String firstName = resultSet.getString("FIRST_NAME");
//				String lastName = resultSet.getString("LAST_NAME");
//				list.add(new RequestQueryStructure(RegistrationNumber, firstName , lastName, id ));
//			}
//			resultSet.close();
//			preparedStatement.close();			
//		} catch (SQLException e) {
//			logger.log(Level.SEVERE, "Failed to create statement in getDataFromBuffer", e);
//			close();
//			System.exit(0);
//		}
		return query.getResultList();
	}
	

	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
	

}
