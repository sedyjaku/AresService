package database;

import java.util.Date;
import java.util.logging.Logger;

import javax.persistence.*;

import aresUpdate.AresUpdate;
import model.CzCompany;
import model.CzCompanyAddress;
import model.CzCompanyInRegister;
import model.CzPersonAddress;
import model.CzPersonAddressComp;
import model.CzPersonInCompany;
import model.CzSubscriberAresBuffer;

public class EntityUploader {
	
	private static Logger logger;
	EntityManager entityManager;
	
	/**
	 * sets EntityManager
	 * @param entityManager EntityManager to set
	 */
	public EntityUploader(EntityManager entityManager)
	{
		setUpLogger();
		this.entityManager=entityManager;
	}
	

	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
	
	/**
	 * Sets all entities related to persist
	 * @param company Company entity to be persisted with all its relations
	 */
	public void insertCompanyAndItsRelations(CzCompany company)
	{
		entityManager.persist(company);
		for(CzPersonInCompany personInCompany : company.getCzPersonInCompanies())
	    {
	    	entityManager.persist(personInCompany.getCzPerson());
	    	entityManager.persist(personInCompany);
	    	for(CzPersonAddress personAddressRel : personInCompany.getCzPerson().getCzPersonAddresses())
	    	{
	    		entityManager.persist(personAddressRel.getCzAddress());
	    		entityManager.persist(personAddressRel);
	    	}
	    }
	    for(CzPersonAddressComp personAddressCompRel : company.getCzPersonAddressComps())
	    {
	    	System.out.println("personAddress");
	    	entityManager.persist(personAddressCompRel);
	    	personAddressCompRel.getCzPerson().printPerson();
	    	personAddressCompRel.getCzAddress().printAddress();
	    	
	    }  
	    for(CzCompanyInRegister reg : company.getCzCompanyInRegisters())
	    {
	    	entityManager.persist(reg);
	    }
	    for(CzCompanyAddress companyAddressRel : company.getCzCompanyAddresses())
	    {
//	    	System.out.println(company.getCzCompanyAddresses().size);
//	    	System.out.println(i);
//	    	System.out.println("companyAddress");
	    	companyAddressRel.getCzAddress().printAddress();
	    	entityManager.persist(companyAddressRel.getCzAddress());
	    	entityManager.persist(companyAddressRel);	
    	}
	
	}
	
	/**
	 * Updates buffer based on BufferResultTypes
	 * @param subscriberId ID of buffer to update
	 * @param processFlag Flag from BufferResultType
	 */
	public void updateBuffer(long subscriberId, char processFlag) {
		Query query = entityManager.createQuery("SELECT b "
				+ "FROM CzSubscriberAresBuffer b "
				+ "WHERE ?1=b.subscriberId",CzSubscriberAresBuffer.class);
		query.setParameter(1, subscriberId);
		CzSubscriberAresBuffer buffer = (CzSubscriberAresBuffer) query.getSingleResult();
		buffer.setBufferResultType(processFlag+"");
		buffer.setProcessingDate(new Date());
    	entityManager.persist(buffer);
		
	}
}
