package queries;

import java.io.IOException;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import aresUpdate.AresUpdate;
import database.EntitySeeker;
import model.*;
import proxy.Proxy;
import us.codecraft.xsoup.Xsoup;

public class RZPQuery extends InformationCollector{

	private static final String XPATH_REQUEST_FOR_ROLE = "D_Role";
	private static final String XPATH_REQUEST_FOR_BIRTHDAY = "D_DN";
	private static final String XPATH_REQUEST_FOR_PERSON = "/are_Ares_odpovedi/are_Odpoved/D_Vypis_RZP/D_Osoby/D_Osoba";
	private static final String XPATH_REQUEST_FOR_LEGAL_FORM = "/are_Ares_odpovedi/are_Odpoved/D_Vypis_RZP/D_ZAU/D_PF";
	private static final String XPATH_REQUEST_FOR_ADDRESSES = "/are_Ares_odpovedi/are_Odpoved/D_Vypis_RZP/D_ADRESY/D_A";
	private static final String XPATH_REQUEST_FOR_COMPANY_NAME = "/are_Ares_odpovedi/are_Odpoved/D_Vypis_RZP/D_ZAU/D_OF";
	private static final String XPATH_QUERY_RETURN_CODE = "/are_Ares_odpovedi/are_Odpoved/D_VH/D_K";
	private static final String XPATH_REQUEST_FOR_FIRST_NAME = "D_J";
	private static final String XPATH_REQUEST_FOR_LAST_NAME = "D_P";
	private static final String XPATH_REQUEST_FOR_ADDRESS_CHECK = "D_B";
	private static final String XPATH_REQUEST_FOR_CITY = "D_N";
	private static final String XPATH_REQUEST_FOR_STRET = "D_NU";
	private static final String XPATH_REQUEST_FOR_DISTRICT = "D_NCO";
	private static final String XPATH_REQUEST_FOR_HOUSE_NUMBER = "D_CD";
	private static final String XPATH_REQUEST_FOR_ORIENTATION_NUMBER = "D_CO";
	private static final String XPATH_REQUEST_FOR_POSTAL_CODE = "D_PSC";
	private static final String XPATH_REQUEST_FOR_LEGAL_FORM_CODE = "D_KPF";
	private static final String XPATH_REQUEST_FOR_LEGAL_FORM_SIMPLE = "D_PFO";
	private static Logger logger;
	
//	private CzAddress address;
//	private CzPerson person;
	private EntitySeeker entitySeeker;
	
	/**
	 * Sets logger
	 */
	public RZPQuery( EntitySeeker entitySeeker)
	{
		this.entitySeeker=entitySeeker;
		setUpLogger();
	}
	
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
	

	/**
	 * Requests a query for ares rzp query
	 * @param sub Query data for request and compare person
	 * @return Company structure
	 */
	public CzCompany getNext(Subscriber sub)
	{
		
			Proxy.setProxy();	
			String query = "http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_rzp.cgi?ico="+sub.getRegistrationNumber()+"&xml=0&rozsah=0&stdadr=true";
			logger.log(Level.INFO, "RZP Query for Registration Number " + sub.getRegistrationNumber()+ "("+ query + ")");
			if(sub.getRegistrationNumber().length()!=REGISTRATION_NUMBER_LENGTH)
			{
				logger.log(Level.WARNING, "Registration number " + sub.getRegistrationNumber() + " has length " + sub.getRegistrationNumber().length() + " and it should have " + REGISTRATION_NUMBER_LENGTH);
				return new CzCompanyDummy();
			}
			//Download and parse xml
			try {
				doc = Jsoup.connect(query).get();
			} catch (IOException e) {
				logger.log(Level.SEVERE, "Failed to connect to ARES in RZP Query", e);
				return null;
			}
			//replacing all elements names from ":" to "_" (":" is special symbol in xpath)
			renameElement(doc.child(0));
			String queryResult = Xsoup.compile(XPATH_QUERY_RETURN_CODE).evaluate(doc).getElements().get(0).text();
			if(Integer.parseInt(queryResult)!=1)
			{
				logger.log(Level.WARNING, "RZP Query for registration number " + sub.getRegistrationNumber() + " did not return 1.");
				return new CzCompanyDummy();
			}
			//Collecting info about company
			String companyName = getCompanyName(doc);						
			Elements elements = Xsoup.compile(XPATH_REQUEST_FOR_LEGAL_FORM).evaluate(doc).getElements();
			String KPF=getKPF(elements);
			String PFO=getPFO(elements);
			CzCompany company = createCompanyEntity(sub.getRegistrationNumber(),companyName,KPF);
			if(company.existsInDatabase())
			{
				logger.log(Level.INFO,"returing company because it was found");
				return company;
			}
			List<CzAddress> addressList=new ArrayList<>();
			
			//Collecting info from person elements
			elements = Xsoup.compile(XPATH_REQUEST_FOR_PERSON).evaluate(doc).getElements();
			for(Element e : elements)
			{
				String role = getRole(e);
				String firstName = getName(e);
				String lastName = getSurname(e);
				String birthDate = getBirthDay(e);
				CzPerson person = createPersonEntity(firstName,lastName,birthDate);
				if(hasPersonAddress(e))
				{
					String cityName = getCityName(e);
					String districtName = getDistrictName(e);
					String streetName = getStreetName(e);
					String houseNumber = getHouseNumber(e);
					String orientationNumber = getOrientationNumber(e);
					String postalCode = getPostalCode(e);
					CzAddress address = createAddressEntity(cityName,districtName,streetName, houseNumber, orientationNumber, postalCode);
					System.out.println("Has person address");
					createPersonAddressRelation(person, address);
					createPersonCompanyAddressRelation(person, company, address);
					addressList.add(address);
				}
				person.checkIfRight(sub.getFirstName(),sub.getLastName());
				createPersonCompanyRelation(person, company, role);

			}
			
			//Collecting info from address element
//			List<Address> addresses = new ArrayList<>();
			elements = Xsoup.compile(XPATH_REQUEST_FOR_ADDRESSES).evaluate(doc).getElements();
			for(Element e : elements)
			{
				String cityName = getCityName(e);
				String districtName = getDistrictName(e);
				String streetName = getStreetName(e);
				String houseNumber = getHouseNumber(e);
				
				String orientationNumber = getOrientationNumber(e);
				String postalCode = getPostalCode(e);
				
				CzAddress address=createAddressEntity(cityName, districtName, streetName, houseNumber, orientationNumber, postalCode);
				if(addressList.contains(address))
				{
					logger.log(Level.INFO, "OBSAHUJE TUTO ADRESU OD PERSON");
				}
				createCompanyAddressRelation(company, address);
//				addresses.add(address);
			}	
			logger.log(Level.INFO, "Adress size:" + company.getCzCompanyAddresses().size());

			return company;
	}
	
	

	private CzCompany createCompanyEntity(String registrationNumber, String companyName, String kPF) {
		CzCompany company=entitySeeker.findCompany(registrationNumber);
		if (company==null)
		{
			company = new CzCompany(registrationNumber,companyName,kPF);
		}
		else
		{
			company.setExists(true);
		}
		return company;
	}
	
	private CzPerson createPersonEntity(String firstName, String lastName, String birthDay) {
		DateFormat format = new SimpleDateFormat("yyyy-M-d", Locale.ENGLISH);
		Date birthDate = null;
		try {
			birthDate = new Date(format.parse(birthDay).getTime());
		} catch (ParseException e) {
			logger.log(Level.WARNING, "failed to parse String to date in person constructor", e);
		}
		CzPerson person=entitySeeker.findPerson(firstName,lastName,birthDate);
		if (person == null)
		{
			person = new CzPerson (firstName,lastName,birthDate) ;
		}
		return person;
	}
	

	
	private CzAddress createAddressEntity(String cityName, String districtName, String streetName, String houseNumber,
			String orientationNumber, String postalCode) {
		CzAddress address=entitySeeker.findAddress(cityName, districtName, streetName, houseNumber, orientationNumber, postalCode);
		if (address==null)
		{
			address = new CzAddress(cityName, districtName, streetName, houseNumber, orientationNumber, postalCode);
		}
		return address;
	}
	
	private void createPersonCompanyRelation(CzPerson person,CzCompany company, String role)
	{
		CzPersonInCompany personInCompanyRel=new CzPersonInCompany();
		personInCompanyRel.setRoleType(role);
		personInCompanyRel.setCzCompany(company);
		personInCompanyRel.setCzPerson(person);
		person.addCzPersonInCompany(personInCompanyRel);
		company.addCzPersonInCompany(personInCompanyRel);
	}
	
	private void createPersonCompanyAddressRelation(CzPerson person, CzCompany company, CzAddress address)
	{
		CzPersonAddressComp personAddressCompanyRel=new CzPersonAddressComp();
		personAddressCompanyRel.setCzAddress(address);
		personAddressCompanyRel.setCzCompany(company);
		personAddressCompanyRel.setCzPerson(person);
		person.addCzPersonAddressComp(personAddressCompanyRel);
		company.addCzPersonAddressComp(personAddressCompanyRel);
		address.addCzPersonAddressComp(personAddressCompanyRel);
	}
	
	private void createCompanyAddressRelation(CzCompany company, CzAddress address)
	{
//		System.out.println("Tato adresa se ted nastavuje:");
//		address.printAddress();
		CzCompanyAddress companyAddressRel=new CzCompanyAddress();
		companyAddressRel.setCzAddress(address);
		companyAddressRel.setCzCompany(company);
		company.addCzCompanyAddress(companyAddressRel);
		address.addCzCompanyAddress(companyAddressRel);
	}
	
	private void createPersonAddressRelation(CzPerson person, CzAddress address)
	{
		CzPersonAddress personAddressRel = new CzPersonAddress();
		personAddressRel.setCzAddress(address);
		personAddressRel.setCzPerson(person);
		person.addCzPersonAddress(personAddressRel);
		address.addCzPersonAddress(personAddressRel);
	}
	
//	public CzAddress getAddress() {
//		return address;
//	}
//	public void setAddress(CzAddress address) {
//		this.address = address;
//	}
//	public CzPerson getPerson() {
//		return person;
//	}
//	public void setPerson(CzPerson person) {
//		this.person = person;
//	}
	/**
	 * checks if person in XML has address
	 * @param e element person to be checked from
	 * @return true if it has address
	 */
	private boolean hasPersonAddress(Element e) {
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_ADDRESS_CHECK).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	/**
	 * Gets Role from xml file by xpath
	 * @param e XML tree
	 * @return Value of role element
	 */
	private String getRole(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_ROLE).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: Role");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets name from xml file by xpath
	 * @param e XML tree
	 * @return Value of name element
	 */
	private String getName(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_FIRST_NAME).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: First Name");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets surname from xml file by xpath
	 * @param e XML tree
	 * @return Value of surname element
	 */
	private String getSurname(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_LAST_NAME).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: Surname");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets birthday from xml file by xpath
	 * @param e XML tree
	 * @return Value of birthday element
	 */
	private String getBirthDay(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_BIRTHDAY).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: Birthday");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets name of city from xml file by xpath
	 * @param e XML tree
	 * @return Value of city name element
	 */
	private String getCityName(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_CITY).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: City Name");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets name of district from xml file by xpath
	 * @param e XML tree
	 * @return Value of district name element
	 */
	private String getDistrictName(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_DISTRICT).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: District Name");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets name of a street from xml file by xpath
	 * @param e XML tree
	 * @return Value of street name element
	 */
	private String getStreetName(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_STRET).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: Street Name");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets number of a house from xml file by xpath
	 * @param e XML tree
	 * @return Value of number house element
	 */
	private String getHouseNumber(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_HOUSE_NUMBER).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: House Number");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets orientation number of a house from xml file by xpath
	 * @param e XML tree
	 * @return Value of number house element
	 */
	private String getOrientationNumber(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_ORIENTATION_NUMBER).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: Orientation Number");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets postal code from xml file by xpath
	 * @param e XML tree
	 * @return Value of postal code element
	 */
	private String getPostalCode(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_POSTAL_CODE).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: Postal Code");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets name of a company from xml file by xpath
	 * @param e XML tree
	 * @return Value of company name element
	 */
	private String getCompanyName(Element e)
	{
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_COMPANY_NAME).evaluate(e).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: Company Name");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets Legal form code from xml file by xpath
	 * @param elements XML tree
	 * @return Value of legal form code element
	 */
	private String getKPF(Elements elements)
	{
		if(elements.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: Elements recieved by getKPF");
			return null;
		}
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_LEGAL_FORM_CODE).evaluate(elements.get(0)).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: KPF");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}
	/**
	 * Gets Legal form simple from xml by xpath
	 * @param elements XML tree
	 * @return Value of legal form elemnt
	 */
	private String getPFO(Elements elements)
	{
		if(elements.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: Elements recieved by getPFO");
			return null;
		}
		Elements tmp = Xsoup.compile(XPATH_REQUEST_FOR_LEGAL_FORM_SIMPLE).evaluate(elements.get(0)).getElements();
		if(tmp.isEmpty())
		{
			logger.log(Level.INFO, "EMPTY: PFO");
			return null;
		}
		else
		{
			return tmp.get(0).text();
		}
	}	
	
}
