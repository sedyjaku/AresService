package queries;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import aresUpdate.AresUpdate;
import informatonStoring.Registers;
import informatonStoring.RegistersDummy;
import proxy.Proxy;
import us.codecraft.xsoup.Xsoup;

public class StandardQuery extends InformationCollector{
	
	private final String XPATH_REQUEST="/are_Ares_odpovedi/are_Odpoved/are_Zaznam/are_Priznaky_subjektu";
	private final String XPATH_QUERY_RETURN_CODE = "/are_Ares_odpovedi/are_Odpoved/are_Pocet_zaznamu";
	
	private static Logger logger;
	
	public StandardQuery()
	{
		setUpLogger();
	}
	
	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
	/**
	 * Request a query for Standard query
	 * @param registrationNumber Registration number of a company to check
	 * @return Collected data
	 */
	public Registers getNext(String registrationNumber)
	{
		Proxy.setProxy();
		String query = "http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_std.cgi?ico="+registrationNumber+"&jazyk=cz&xml=0";
		logger.log(Level.INFO, "Standard Query for Registration Number " + registrationNumber + "(" + query + ")");
		if(registrationNumber.length()!=REGISTRATION_NUMBER_LENGTH)
		{
			logger.log(Level.WARNING, "Registration number " + registrationNumber + " has length " + registrationNumber.length() + " and it should have " + REGISTRATION_NUMBER_LENGTH);
			return new RegistersDummy();
		}		
		//download and Parse XML
		try {
			doc = Jsoup.connect(query).get();
		} catch (IOException e) {
			logger.log(Level.SEVERE, "Failed to connect to ARES in Standard Query", e);
			return null;
		}
		//replacing all elements names from ":" to "_" (":" is special symbol in xpath)
		renameElement(doc.child(0));
		String queryResult = Xsoup.compile(XPATH_QUERY_RETURN_CODE).evaluate(doc).getElements().get(0).text();
		if(Integer.parseInt(queryResult)==0)
		{
			logger.log(Level.INFO, "Companies found for registration number " + registrationNumber + " is 0");
			return new RegistersDummy();
		}
		Elements elements = Xsoup.compile(XPATH_REQUEST).evaluate(doc).getElements();
		return new Registers(elements.get(0).text());
	}



}
