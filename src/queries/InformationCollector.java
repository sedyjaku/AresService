package queries;


import java.util.logging.Logger;



import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import aresUpdate.AresUpdate;


public class InformationCollector {

	
	
	protected Document doc;
	protected static Logger logger;
	public final int REGISTRATION_NUMBER_LENGTH=8;

	
	public InformationCollector()
	{
		setUpLogger();
	}
	
	/**
	 * sets up logger for this class and parent to AresUpdate
	 */
	protected void setUpLogger() {
		logger= Logger.getLogger( this.getClass().getName());
		logger.setParent(Logger.getLogger(AresUpdate.class.getName()));
		
	}
	
	public void renameElement(Element e)
	{
		String str=e.tagName();
		e=e.tagName(str.replaceAll(":", "_"));
		
//		System.out.println(str + " přejmenován na " + e.tagName());
		for(int i=0;i<e.children().size();i++)
		{
			renameElement(e.child(i));
		}
	}

		


	
}
